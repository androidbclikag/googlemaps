package com.example.googlemaps

import android.content.res.Resources
import android.os.Bundle
import android.util.Log.d
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import kotlinx.android.synthetic.main.activity_maps.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        init()

    }


    private fun init() {
        mMap.setOnCameraMoveStartedListener {
            d("start", "start")
            start()
        }
        mMap.setOnCameraIdleListener {
            end()
            d("end", "end")
        }
    }


    private fun start() {
        borderImageView.visibility = View.GONE
        pinImageView.animate().translationY(-pxFromDp(20F))


    }

    private fun end() {
        pinImageView.animate().translationY(0F)
        borderImageView.visibility = View.VISIBLE
    }

    private fun pxFromDp(dp: Float): Float {
        return dp * Resources.getSystem().displayMetrics.density
    }


}
